<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        // permission, sementara dibikin simple, belum sempet bikin dinamis
        $permissionAdmin = App\Permission::create(['name' => 'access-admin']);
        $permissionGuru = App\Permission::create(['name' => 'access-guru']);
        $permissionSiswa = App\Permission::create(['name' => 'access-siswa']);

        // bikin role
        $roleRoot = App\Role::create(['name' => 'admin.root']);
        $roleRoot->addPermission($permissionSiswa);
        $roleRoot->addPermission($permissionGuru);
        $roleRoot->addPermission($permissionAdmin);

        $roleAdmin = App\Role::create(['name' => 'admin']);
        $roleAdmin->addPermission($permissionAdmin);

        $roleGuru = App\Role::create(['name' => 'guru']);
        $roleGuru->addPermission($permissionGuru);

        $roleSiswa = App\Role::create(['name' => 'siswa']);
        $roleSiswa->addPermission($permissionSiswa);

        // generate users untuk masing-masing role

        // NOTE variable yg menampung user value nya null, tapi data udah masuk ke db
        // TODO make sure to fix these

        // create super admin
        $root = App\User::create([
            'name' => 'Administrator',
            'username' => 'root',
            'email' => 'root@bensu.com',
            'password' => bcrypt('llA#kc0lnU'),
            'remember_token' => str_random(10),
        ])->assignRole($roleRoot);

        // generate admin
        $admin = factory(App\User::class)->create()->assignRole($roleAdmin);

        // generate 5 siswa
        $siswa = factory(App\User::class, 5)->create()
            ->each(function($u) use ($roleSiswa){
                $u->assignRole($roleSiswa);
            });

        // generate 5 guru
        $guru = factory(App\User::class, 5)->create()
            ->each(function($u) use ($roleGuru){
                $u->assignRole($roleGuru);
            });
    }
}
