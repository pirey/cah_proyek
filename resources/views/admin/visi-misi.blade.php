@extends('admin.layouts.admin')

@section('content')
    <!-- Main content -->
    <section class="content">

        <!-- Your Page Content Here -->
        <div class="box box-primary">
            <!--<div class="box-header with-border">
                <h3 class="box-title">Visi Misi</h3>
            </div>-->
            <!-- /.box-header -->
            <!-- form start -->
            <!--<form method="post" action="{{ url('admin/visi-misi') }}" role="form">-->

            {!! Form::model( $visiMisi, ['url' => 'admin/visi-misi', 'method' => 'PUT'] ) !!}

                <div class="box-body">
                    <div class="form-group {!! $errors->has('title') ? 'has-error' : '' !!}">
                        <label for="title">Judul Halaman</label>
                        {!! Form::text('title', null, ['class'=>'form-control', 'id' => 'title', 'placeholder' => 'Judul']) !!}

                        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {!! $errors->has('body') ? 'has-error' : '' !!}">
                        <label for="body">Konten</label>
                        {!! Form::textarea('body', null, ['class'=>'text-editor', 'id' => 'body', 'placeholder' => 'Konten...']) !!}

                        {!! $errors->first('body', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="checkbox">
                        <label>
                            {!! Form::checkbox('status', 1) !!} Aktif
                        </label>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            {!! Form::close() !!}

            <!--</form>-->
        </div>

    </section>
    <!-- /.content -->
@endsection
