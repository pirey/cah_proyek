<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenericContent extends Model
{
    protected $fillable = ['name', 'title', 'body', 'status'];
}
