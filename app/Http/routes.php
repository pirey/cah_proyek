<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');

// Admin Routes...
Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {

    // auth stuff
    Route::get('login', 'AuthController@showLoginForm');
    Route::post('login', 'AuthController@login');
    Route::get('logout', 'AuthController@logout');

    // administration
    Route::get('/', 'DashboardController@index');

    Route::group(['middleware' => 'auth.admin'], function() {

        Route::get('/visi-misi', function() {
            // get the content of visi misi
            $visiMisi = App\GenericContent::where('name', 'visimisi')->first();

            return view('admin.visi-misi', ['visiMisi' => $visiMisi]);
        });

        Route::put('/visi-misi', function() {

            $validator = Validator::make(request()->all(), [
                'title' => 'required|max:255',
                'body' => 'required'
            ]);

            if ($validator->fails()) {
                return redirect('admin/visi-misi')->withErrors($validator->messages());
            }
            // update content of visi-misi
            $visiMisi = App\GenericContent::firstOrNew(['name' => 'visimisi']);
            $visiMisi->title = request('title');
            $visiMisi->body = request('body');
            $visiMisi->status = request('status') || false;
            $visiMisi->save();

            return redirect('admin/visi-misi');
        });

    });

});
