<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // cek apakah user sudah login
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('admin/login');
            }
        }

        // periksa apakah user adalah admin / guru
        $user = Auth::user();
        if ($user->hasRole('guru') || $user->isAdmin()) {
            return $next($request);
        }

        // not authorized
        return abort(403);
        //return redirect()->guest('admin/login');
    }
}
