<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function isAdmin()
    {
        if ($this->isRoot()) {
            return true;
        }

        if ($this->roles->contains('name', 'admin')) {
            return true;
        }

        return false;
    }

    public function isRoot()
    {
        if ($this->roles->contains('name', 'admin.root')) {
            return true;
        }

        return false;
    }

    public function hasRole($role)
    {
        if ($this->isAdmin()) {
            return true;
        }

        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }

        return !! $this->roles->intersect($role)->count();
    }

    public function assignRole($role)
    {
        if (is_string($role)) {
            $role = Role::where('name', $role)->firstOrFail();
        }

        return $this->roles()->attach($role);
    }

    public function revokeRole($role)
    {
        if (is_string($role)) {
            $role = Role::where('name', $role)->firstOrFail();
        }

        return $this->roles()->detach($role);
    }
}
